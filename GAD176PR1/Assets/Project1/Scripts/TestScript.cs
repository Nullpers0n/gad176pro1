﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
	
	private Rigidbody2D _rb2D;
	[SerializeField] private float movementSpeed = 10f;
	private float playerspeed = 60f;
	
    // Start is called before the first frame update
    void Start()
    {
	    _rb2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
	{
    	
		//do a null check
    	
	    if (Input.GetKeyDown(KeyCode.D)) { _rb2D.AddForce(transform.right * playerspeed); Debug.Log("adding force right");}
	    if (Input.GetKeyDown(KeyCode.A)) _rb2D.AddForce(transform.right * playerspeed * -1f);
		
    }
}
